#CXX=x86_64-w64-mingw32-g++-win32
#CXXFLAGS=-O3 -static -L/home/davalli/Scaricati/SDL2-2.28.5/x86_64-w64-mingw32/lib -I/home/davalli/Scaricati/SDL2-2.28.5/x86_64-w64-mingw32/include -lmingw32 -lSDL2main -lSDL2 -mwindows -lm -ldinput8 -ldxguid -ldxerr8 -luser32 -lgdi32 -lwinmm -limm32 -lole32 -loleaut32 -lshell32 -lsetupapi -lversion -luuid
CXX=g++
CXXFLAGS=-O3 -lSDL2

all: main.cpp invader.cpp invader.h ship.cpp ship.h laser.cpp laser.h game.cpp game.h
	$(CXX) main.cpp invader.cpp ship.cpp laser.cpp game.cpp -o invaders $(CXXFLAGS)

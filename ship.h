#pragma once

#include <SDL2/SDL.h>

class Ship
{
private:
	SDL_Point Points[4];
public:

	Ship(int x,int y):
	Points{{x,y},{x+30,y+30},{x-30,y+30},{x,y}}
	{
	}

	void Move(int x,int y);

	void Draw(SDL_Renderer* Renderer)const
	{
		SDL_SetRenderDrawColor(Renderer,0,255,0,0);
		SDL_RenderDrawLines(Renderer,Points,4);
	}

	unsigned GetPos(void)const
	{
		return Points[0].x;
	}
};

#include "invader.h"

bool Invader::CanMove(int x,int y)const
{
	if(-x>Rect.x) return false;
	if(-y>Rect.y) return false;
	if(Rect.x+Rect.w+x>=640) return false;
	if(Rect.y+Rect.h+y>=440) return false;
	return true;
}

bool Invader::CheckCollision(unsigned x,unsigned y)const
{
	if(x<Rect.x) return false;
	if(y<Rect.y) return false;
	if(x>Rect.x+Rect.w) return false;
	if(y>Rect.y+Rect.h) return false;
	return true;
}

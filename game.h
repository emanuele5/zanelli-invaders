#pragma once

#include "invader.h"
#include "laser.h"
#include "ship.h"
#include <list>

class Game
{
	// game configuration
	static const int LasersMax=3;		// max number of lasers in the scene
	static const unsigned LevelsMax=9;	// max number of levels
	static const unsigned ShipSpeed=5;	// horizontal speed of the user ship
	static const unsigned LaserSpeed=10;	// speed of the lasers
	
	unsigned InvaderSpeed{0};	// horizontal speed of the invaders
	
	// game objects
	Ship* UserShip{nullptr};
	std::list<Invader> Invaders;
	std::list<Laser> Lasers;
	
	// SDL data
	SDL_Renderer* Renderer;

	void ClearScreen(unsigned r,unsigned g,unsigned b)const;
	void EndLevel(bool won);
	bool AdvanceInvaders(void);
	bool GenerateLevel(unsigned level);
	
	public:
	explicit Game(SDL_Renderer* renderer):
	Renderer(renderer)
	{
	}

	bool GameLoop(unsigned level);
};

#pragma once

#include <SDL2/SDL.h>

class Invader
{
private:
	SDL_Rect Rect;
public:

	Invader(int x,int y):
	Rect{x,y,25,15}
	{
	}

	bool CanMove(int x,int y)const;

	void Move(int x,int y)
	{
		Rect.x+=x;
		Rect.y+=y;
	}

	bool CheckCollision(unsigned x,unsigned y)const;

	void Draw(SDL_Renderer* Renderer)const
	{
		SDL_SetRenderDrawColor(Renderer,0,0,255,0);
		SDL_RenderFillRect(Renderer,&Rect);
	}
};

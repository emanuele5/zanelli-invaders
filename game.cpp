#include "game.h"

void Game::ClearScreen(unsigned r,unsigned g,unsigned b)const
{
	SDL_SetRenderDrawColor(Renderer,r,g,b,0);
	SDL_RenderClear(Renderer);
}

void Game::EndLevel(bool won)
{
	// draw the game-over screen
	if(won) ClearScreen(0,255,0);
	else ClearScreen(255,0,0);
	
	delete UserShip;
}

bool Game::AdvanceInvaders(void)
{
	for(auto i=Invaders.begin();i!=Invaders.end();++i)
	{
		if((*i).CanMove(0,5)==false) return false;
	}
	for(auto i=Invaders.begin();i!=Invaders.end();++i) (*i).Move(0,5);
	return true;
}

bool Game::GenerateLevel(unsigned level)
{
	UserShip=new Ship(320,440);

	switch(level)
	{
	default:
		// add custom levels here
		return false;
	case 8:
		for(int i=0;i<10;i++) Invaders.emplace_front(20+60*i,70);
	case 7:
		for(int i=0;i<10;i++) Invaders.emplace_front(50+60*i,40);
	case 6:
		for(int i=0;i<10;i++) Invaders.emplace_front(20+60*i,10);
		InvaderSpeed=5;
		break;
	case 5:
		for(int i=0;i<10;i++) Invaders.emplace_front(20+60*i,70);
	case 4:
		for(int i=0;i<10;i++) Invaders.emplace_front(50+60*i,40);
	case 3:
		for(int i=0;i<10;i++) Invaders.emplace_front(20+60*i,10);
		InvaderSpeed=3;
		break;
	case 2:
		for(int i=0;i<10;i++) Invaders.emplace_front(20+60*i,70);
	case 1:
		for(int i=0;i<10;i++) Invaders.emplace_front(50+60*i,40);
	case 0:
		for(int i=0;i<10;i++) Invaders.emplace_front(20+60*i,10);
		InvaderSpeed=2;
		break;
	}
	// clear lasers
	Lasers.clear();
	return true;
}

bool Game::GameLoop(unsigned level)
{
	if(GenerateLevel(level)==false) return false;
	while(1)
	{
		// handle exit events
		SDL_Event Event;
		while(SDL_PollEvent(&Event)) if(Event.type==SDL_QUIT) exit(0);

		// handle keyboard events
		auto KeyState=SDL_GetKeyboardState(NULL);
		if(KeyState[SDL_SCANCODE_LEFT]) UserShip->Move(-ShipSpeed,0);
		if(KeyState[SDL_SCANCODE_RIGHT]) UserShip->Move(ShipSpeed,0);
		if(KeyState[SDL_SCANCODE_SPACE])
		{
			static unsigned FireLag=0; // just a bit of lag between laser firings
			if(Lasers.size()<LasersMax&&(FireLag%7)==0) Lasers.emplace_front(UserShip->GetPos(),440);
			FireLag++;
		}

		// move invaders
		static bool MoveLeft=false;
		int Movement=InvaderSpeed; // invader movement offset
		if(MoveLeft) Movement=-Movement;
		for(auto i=Invaders.begin();i!=Invaders.end();++i)
		{
			if((*i).CanMove(Movement,0)==false)
			{
				if(MoveLeft==true) MoveLeft=false;
				else MoveLeft=true;
				// advance if border reached
				if(AdvanceInvaders()==false)
				{
					EndLevel(false);
					return false;
				}
				break;
			}
		}
		for(auto i=Invaders.begin();i!=Invaders.end();++i) (*i).Move(Movement,0);

		// move lasers
		for(auto i=Lasers.begin();i!=Lasers.end();++i)
		{
			// delete the laser if border reached
			if((*i).CanMove(0,-LaserSpeed)==false)
			{
				LaserDelete:
				auto d=i--;
				Lasers.erase(d);
				continue;
			}
			else (*i).Move(0,-LaserSpeed);
			for(auto j=Invaders.begin();j!=Invaders.end();++j)
			{
				if((*i).CheckHit(*j)==true)
				{
					// HIT! now delete both the laser and the invader
					auto e=j--;
					Invaders.erase(e);
					goto LaserDelete; // please forgive me for this
				}
			}
		}
		if(Invaders.empty())
		{
			EndLevel(true);
			return true;
		}

		ClearScreen(0,0,0);
		// draw lasers
		for(auto i=Lasers.begin();i!=Lasers.end();++i) (*i).Draw(Renderer);
		// draw ship
		UserShip->Draw(Renderer);
		// draw invaders
		for(auto i=Invaders.begin();i!=Invaders.end();++i) (*i).Draw(Renderer);

		// draw all to screen and wait
		SDL_RenderPresent(Renderer);
		SDL_Delay(20);
	}
}

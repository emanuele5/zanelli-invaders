#include "laser.h"

bool Laser::CanMove(int x,int y)const
{
	if(-x>Points[0].x) return false;
	if(-y>Points[1].y) return false;
	if(Points[0].x+x>=640) return false;
	if(Points[0].y+y>=480) return false;
	return true;
}

bool Laser::CheckHit(const Invader& Target)const
{
	if(Target.CheckCollision(Points[0].x,Points[0].y)==true) return true;
	if(Target.CheckCollision(Points[1].x,Points[1].y)==true) return true;
	return false;
}

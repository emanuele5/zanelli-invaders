#include "game.h"

static void WaitForExit(void)
{
	SDL_Event Event;
	while(SDL_WaitEvent(&Event)) if(Event.type==SDL_QUIT) exit(0);
}

int main(int argc,char** argv)
{
	// init sdl
	SDL_Init(SDL_INIT_VIDEO);
	SDL_Window* MainWindow=SDL_CreateWindow("Invaders",SDL_WINDOWPOS_CENTERED,SDL_WINDOWPOS_CENTERED,640,480,0);
	SDL_Renderer* MainRenderer=SDL_CreateRenderer(MainWindow,-1,0);

	Game game(MainRenderer);
	for(int i=0;game.GameLoop(i);i++)
	{
	}
	SDL_RenderPresent(MainRenderer);
	WaitForExit();
}

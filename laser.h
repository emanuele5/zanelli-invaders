#pragma once

#include <SDL2/SDL.h>
#include "invader.h"

class Laser
{
private:
	SDL_Point Points[2];
public:

	Laser(int x,int y):
	Points{{x,y},{x,y-10}}
	{
	}

	bool CanMove(int x,int y)const;

	void Move(int x,int y)
	{
		Points[0].x+=x;
		Points[0].y+=y;
		Points[1].x+=x;
		Points[1].y+=y;
	}

	bool CheckHit(const Invader& Target)const;

	void Draw(SDL_Renderer* Renderer)const
	{
		SDL_SetRenderDrawColor(Renderer,255,0,0,0);
		SDL_RenderDrawLines(Renderer,Points,2);
	}
};
